from abc import ABCMeta, abstractmethod


class Encoder(metaclass=ABCMeta):
    @abstractmethod
    def encode(self, data):
        pass


class Decoder(metaclass=ABCMeta):
    @abstractmethod
    def decode(self, data):
        pass


class Producer(metaclass=ABCMeta):
    def __init__(self, encoder: Encoder) -> None:
        self._encoder = encoder

    @abstractmethod
    def _put(self, msg):
        pass

    def put(self, msg):
        msg2 = self._encoder.encode(msg)
        self._put(msg2)


class Consumer(metaclass=ABCMeta):
    def __init__(self, decoder: Decoder) -> None:
        self._decoder = decoder

    @abstractmethod
    def _get(self, timeout: int):
        pass

    def get(self, timeout: int):
        """
        Get a message from the consumer
        :param timeout: How many seconds to wait before giving up. Passing a value of 0 will block the call
        """
        msg = self._get(timeout)
        return self._decoder.decode(msg)
