"""
Uniformized exceptions for mQuake
"""


class MQuakeTimeoutError(TimeoutError, Exception):
    pass
