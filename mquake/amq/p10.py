from proton import Message, Url
from proton.utils import BlockingConnection

from mquake.base import Producer, Consumer, Encoder, Decoder
from mquake.encoders import NullEncoder, NullDecoder


def proton_url_add_user_passwd(url: Url, user: str = None, passwd: str = None) -> Url:
    url.password = passwd
    url.username = user
    return url


class AMQPProducer(Producer):
    def __init__(self, url: str, name: str, encoder: Encoder = NullEncoder()) -> None:
        super().__init__(encoder)
        self._url = url
        self._name = name
        self._sender = None
        self._conn = None

    def __enter__(self):
        self._conn = BlockingConnection(self._url)
        self._sender = self._conn.create_sender(self._name)
        return self

    def __exit__(self, exc_type, exc_val, traceback):
        del self._sender  # Library seems to have GC weirdness
        self._sender = None
        self._conn.close()
        self._conn = None

    def _put(self, msg):
        self._sender.send(Message(body=msg))


class AMQPConsumer(Consumer):
    def __init__(self, url: str, name: str, decoder: Decoder = NullDecoder()) -> None:
        super().__init__(decoder)
        self._url = url
        self._name = name
        self._receiver = None
        self._conn = None

    def __enter__(self):
        self._conn = BlockingConnection(self._url)
        self._receiver = self._conn.create_receiver(self._name)
        return self

    def __exit__(self, exc_type, exc_val, traceback):
        del self._receiver  # Appears to be some GC weirdness so lets be explicit
        self._receiver = None
        self._conn.close()
        self._conn = None

    def _get(self, timeout: int):
        msg = self._receiver.receive(timeout=timeout)
        self._receiver.accept()
        return msg.body
