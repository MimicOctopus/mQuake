import zmq
from zmq.devices.basedevice import ProcessDevice

from mquake.base import Producer, Consumer, Encoder, Decoder
from mquake.encoders import NullEncoder, NullDecoder
from mquake.exception import MQuakeTimeoutError


class ZMQManager:
    """
    Connection manager for ZMQ. Essentially a wrapper around a device
    """

    def __init__(self, address: str):
        self._frontend = "{}/frontend".format(address)
        self._backend = "{}/backend".format(address)
        self._device = ProcessDevice(zmq.STREAMER, zmq.PULL, zmq.PUSH)
        self._device.bind_in(self._frontend)
        self._device.bind_out(self._backend)
        self._device.setsockopt_in(zmq.IDENTITY, b'PULL')
        self._device.setsockopt_out(zmq.IDENTITY, b'PUSH')

    def start(self):
        self._device.start()

    def clean(self):
        # TODO: The device won't shut down unless it is empty
        raise NotImplementedError

    def new_consumer(self) -> 'ZMQConsumer':
        return ZMQConsumer(self._backend)

    def new_producer(self) -> 'ZMQProducer':
        return ZMQProducer(self._frontend)


class ZMQConsumer(Consumer):
    """
    A Consumer that uses ZeroMQ
    """

    def __init__(self, address: str, decoder: Decoder = NullDecoder()) -> None:
        super().__init__(decoder)
        self._address = address
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.PULL)

    def __enter__(self):
        self._socket.connect(self._address)
        return self

    def _get(self, timeout: int):
        _poller = zmq.Poller()
        _poller.register(self._socket, zmq.POLLIN)
        if timeout == 0 or _poller.poll(timeout * 1000):
            return self._socket.recv()
        raise MQuakeTimeoutError

    def __exit__(self, exc_type, exc_val, traceback):
        self._socket.disconnect(self._address)


class ZMQProducer(Producer):
    """
    A Producer that uses ZeroMQ
    """

    def __init__(self, address: str, encoder: Encoder = NullEncoder()) -> None:
        super().__init__(encoder)
        self._address = address
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.PUSH)

    def __enter__(self):
        self._socket.connect(self._address)
        return self

    def _put(self, data):
        return self._socket.send(data)

    def __exit__(self, exc_type, exc_val, traceback):
        self._socket.disconnect(self._address)
