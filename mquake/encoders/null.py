from mquake.base import Encoder, Decoder


class NullEncoder(Encoder):
    def encode(self, data):
        return data


class NullDecoder(Decoder):
    def decode(self, data):
        return data
