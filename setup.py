"""
Setup mQuake
"""

import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_requirements(filename='requirements.txt'):
    requirements = os.path.join(os.path.dirname(__file__), filename)
    with open(requirements) as rf:
        data = rf.read()
    lines = map(lambda s: s.strip(), data.splitlines())
    return [l for l in lines if l and not l.startswith('#')]


setup(
    name='mQuake',
    version="1.0.0",
    author='Max Paradis',
    author_email='max@faeriol.me',
    packages=['mquake'],
    url='http://github.com/faeriol/mQuake',
    license='BSD (see LICENSE)',
    description='Shaking up your Message Queues',
    install_requires=get_requirements(),
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ]
)
